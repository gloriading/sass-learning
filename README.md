# SCSS Learning
* This is a learning repo from Traversy Media https://www.youtube.com/watch?v=nu5mdN2JIwM
* Use VSCode Extension - Live Sass Compiler
* Open VSCode settings, Live Sass Compiler config > settings.json

```json
  "liveSassCompile.settings.excludeList": [
      "**/node_modules/**",
      ".vscode/**"
  ],
  "liveSassCompile.settings.generateMap": false,
  "liveSassCompile.settings.formats": [
      {
          "format": "compressed",
          "savePath": "/css"
      }
  ]
```
